/******************************************************************************
 * Working:  For every malloc call, get the size of malloc if it is a constant\
 *           expression check if the size is a multiple of the size of the \
 *           data type being pointed to. If it is not a constant expression, \
 *           then get the size being multiplied and check if it is a multiple \
 *           of the size to which the pointer points 
 *
 * Authors: Nagarathnam Muthusamy and Pranjal Jumde
 *****************************************************************************/

#include "extend-lang.hpp"     // Extend API

START_EXTEND_CHECKER( s3_malloc, simple );

ANALYZE_TREE()
{
  CallSite malloc("malloc");
  LocalVar lv;
  AnySubpart lv_sp( lv );
  Const_int a;
  Expr b;

  SymBinop s(BIN_MULT, a, b);
  ExpressionPattern &c = Or( ( lv_sp = malloc( a ) ), (lv_sp = malloc( s )));

  if( MATCH( c ) ) {

		const pointer_type_t *typ = lv.get_type()->skip_qualifiers()->\
    as_pointer_p();

		size_t pointedToSize = typ->get_pointed_to()->get_type_size();
    size_t argSize = a.llval();

		if((argSize % pointedToSize) != 0) {
			OUTPUT_ERROR("Malloc bug, Current tree:" << CURRENT_TREE);
		}
  }

}

END_EXTEND_CHECKER();
MAKE_MAIN( s3_malloc )

