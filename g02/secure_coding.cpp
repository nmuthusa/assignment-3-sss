/**
 *
 * Checker Name:        Custom Version of Secure Coding Checker
 * Checker Author:      Nagarathnam Muthusamy and Pranjal Jumde
 * Checker Description: Checks for usage of illegal functions
 * Working: Used Callsite and Match to figure out calls to unsafe functions.
 *
 **/

#include "extend-lang.hpp"

START_EXTEND_CHECKER( secure_coding, simple );

ANALYZE_TREE()
{
  //Calls to the following functions will be unsafe
  CallSite strcpy("strcpy", false);
  CallSite sscanf("sscanf", false);
  CallSite sprintf("sprintf", false);
  CallSite strcat("strcat", false);
  CallSite rand("rand", false);

  if (MATCH(strcat))
  {
      OUTPUT_ERROR("[VERY RISKY]. Using &quot;strcat&quot; can cause a \
      buffer overflow when done incorrectly.  The destination of a strcat() \
      call must have enough space to accept the source. Use strncat() \
      instead.");
  }

  if (MATCH(strcpy))
  {
      OUTPUT_ERROR("[VERY RISKY]. Using &quot;strcpy&quot; can cause a \
      buffer overflow when done incorrectly.  If the destination string \
      of a strcpy() is not large enough then anything might happen. Use \
      strncpy() instead.");
  }

  if (MATCH(rand))
  {
      OUTPUT_ERROR("[VERY RISKY]. Using &quot;rand&quot; can cause a \
      insecure series of predictable numbers when done incorrectly.  \
      rand() is not a very good random number generator.  It uses a 32 bit \
      seed and maintains a 32 bit state.  The output is 32 bits in length \
      making it a simple matter to determine the function's internal state \
      by examining the output.  As a result, rand() is not very random. Use \
      /dev/random or /dev/urandom instead.");
  }

  if (MATCH(sprintf))
  {
      OUTPUT_ERROR("[VERY RISKY]. Using &quot;sprintf&quot; can cause a \
      buffer overflow when done incorrectly.  Because sprintf() assumes an \
      arbitrarily long string, callers must be careful not to overflow the \
      actual space of the destination. Use snprintf() instead, or correct \
      precision specifiers.");
  }

  if (MATCH(sscanf))
  {
      OUTPUT_ERROR("[VERY RISKY]. Using &quot;sscanf&quot; can cause a buffer \
      overflow when done incorrectly.  sscanf() assumes an arbitrarily large \
      string, so callers must use correct precision specifiers or never use \
      sscanf(). Use correct precision specifiers or do your own parsing.");
  }

}

END_EXTEND_CHECKER();
MAKE_MAIN( secure_coding )

