/**
 *
 * Checker Name:        Integer Truncation
 * Checker Author:      Nagarathnam Muthusamy and Pranjal Jumde
 * Checker Description: Checks for integer truncation errors
 *     
 *                      The following cases are considered:
 *
 *                      * implicit cast to a datatype of smaller size
 *                      * conversion from signed to unsigned and vice versa
 *                      * conversion from float to integral
 *
 **/

#include "extend-lang.hpp"

START_EXTEND_CHECKER( s4_truncation, simple );

ANALYZE_TREE()
{
  Scalar src;
  AnyType dst;

  size_t src_size, dst_size;

  //only if cast is automatic it is a integer truncation bug
  Cast implicit(src, dst, Cast::AUTOMATIC);

  const scalar_type_t *src_scalar;

  if ( MATCH(implicit) )
  {
    if ((src_scalar = src.get_type()->skip_qualifiers()->as_scalar_p()))
    {
	      src_size = src.get_type()->get_type_size();
        dst_size = dst.get_type()->get_type_size();

        if(src_size > dst_size)
        {
          OUTPUT_ERROR("Integer Truncation");
        } 
        else if(src_size == dst_size) 
        {
          if(!((src.get_type()->is_unsigned() && dst.is_unsigned()) || \
		      (!src.get_type()->is_unsigned() && !dst.is_unsigned())))
            {
              OUTPUT_ERROR("Integer Truncation");
            }

          if((src.get_type()->is_float() && \
            !dst.get_type()->skip_qualifiers()->as_scalar_p()->is_float()))  
            {
              OUTPUT_ERROR("Integer Truncation");
            }
        }
    }
       
  }

}

END_EXTEND_CHECKER();
MAKE_MAIN( s4_truncation )

