/*
 *  Authors: Nagarathnam Muthusamy and Pranjal Jumde
 *  Checker Name: strncat overflow
 *  Description: Checks for overflow errors for string manipulation functions
 */

#include "extend-lang.hpp"

START_EXTEND_CHECKER( s5_overflow , int_store );

ANALYZE_TREE()
{
    CallSite strncat("strncat");
    Expr var2;
    AnyType point;
    Expr arr;
    Cast var1(arr, point, Cast::ANY);
    Const_int len;

    if ( MATCH ( strncat ( var1, var2, len ) ) ) {
      const pointer_type_t *typ = arr.get_type()->as_pointer_p();
      unsigned int countForVar1 = 0;
      
      if(typ && typ->get_pointed_to() && typ->get_pointed_to()->as_array_p() \
        && typ->get_pointed_to()->as_array_p()->get_element_count()) {
          countForVar1 = typ->get_pointed_to()->as_array_p()->\
          get_element_count();
	    }

	    unsigned int countForLen = len.llval();

	    if(countForVar1 <= countForLen) {
        OUTPUT_ERROR("String Overflow Detected - Analysis: "<<CURRENT_TREE);
      }
   }
}

END_EXTEND_CHECKER();
MAKE_MAIN( s5_overflow )
